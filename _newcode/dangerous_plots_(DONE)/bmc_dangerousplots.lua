print("BLOODMOON: Loaded bmc_dangerousplots.lua...");

local tDangerousTerrains = {};
local tDangerousFeatures = {};

--run at game start
function BMC_ListDangerousPlotTypes()
	print("BLOODMOON: Clearing tDangerousTerrains...");
	tDangerousTerrains = {};
	for row in GameInfo.Terrains() do
		if row.DamagePerTurn > 0 then
			print("BLOODMOON: All plots with " .. row.Type .. " should do " .. row.DamagePerTurn .. " damage per turn.");
			tDangerousTerrains[GameInfoTypes[row.Type]] = row.DamagePerTurn;
		end
	end
	print("BLOODMOON: Clearing tDangerousFeatures...");
	tDangerousFeatures = {};
	for row in GameInfo.Features() do
		if row.DamagePerTurn > 0 then
			print("BLOODMOON: All plots with " .. row.Type .. " should do " .. row.DamagePerTurn .. " damage per turn.");
			tDangerousFeatures[GameInfoTypes[row.Type]] = row.DamagePerTurn;
		end
	end
end

--checks if plot has a dangerous terraintype
function BMC_IsPlotDangerousTerrain(pPlot)
	for eTerrain, _ in pairs(tDangerousTerrains) do
		if pPlot:GetTerrainType() == eTerrain then
			return true;
		end
	end
	return false;
end

--checks if a plot has a dangerous featuretype
function BMC_IsPlotDangerousFeature(pPlot)
	for eFeature, _ in pairs(tDangerousFeatures) do
		if pPlot:GetFeatureType() == eFeature then
			return true;
		end
	end
	return false;
end

--checks if a unit is immune to the dangerous terraintype
function BMC_IsUnitImmuneToPlotTerrain(pUnit, pPlot)
	for row in GameInfo.UnitPromotions() do
		local ePromotion = GameInfoTypes[row.Type];
		local eImmuneTerrain = GameInfoTypes[row.ImmuneTerrainType];
		if pPlot:GetTerrainType() == eImmuneTerrain and pUnit:IsHasPromotion(ePromotion) then
			return true;
		end
	end
	return false;
end

--checks if a unit is immune to the dangerous featuretype
function BMC_IsUnitImmuneToPlotFeature(pUnit, pPlot)
	for row in GameInfo.UnitPromotions() do
		local ePromotion = GameInfoTypes[row.Type];
		local eImmuneFeature = GameInfoTypes[row.ImmuneFeatureType];
		if pPlot:GetFeatureType() == eImmuneFeature and pUnit:IsHasPromotion(ePromotion) then
			return true;
		end
	end
	return false;
end

--runs every time a player turn begins (except turn 0)
function BMC_DoDangerousPlotCheck(iPlayerID)
	local pPlayer = Players[iPlayerID];
	for pUnit in pPlayer:Units() do
		local pPlot = pUnit:GetPlot();
		print("BLOODMOON: Found unit at " .. pPlot:GetX() .. "," .. pPlot:GetY() .. ". Checking for plot damage...");
		local iDamageToDo = 0;
		if BMC_IsPlotDangerousTerrain(pPlot) then
			print("BLOODMOON: Plot has dangerous TerrainType! Checking for unit immunity...");
			if not BMC_IsUnitImmuneToPlotTerrain(pUnit, pPlot) then
				print("BLOODMOON: Unit is not immune to damage from this TerrainType!!!");
				iDamageToDo = iDamageToDo + tDangerousTerrains[pPlot:GetTerrainType()];
				print("BLOODMOON: " .. iDamageToDo .. " damage will be inflicted...");
			else
				print("BLOODMOON: Unit is immune to damage from this TerrainType!!!");
			end
		end
		if BMC_IsPlotDangerousFeature(pPlot) then
			print("BLOODMOON: Plot has dangerous FeatureType! Checking for unit immunity...");
			if not BMC_IsUnitImmuneToPlotFeature(pUnit, pPlot) then
				print("BLOODMOON: Unit is not immune to damage from this FeatureType!!!");
				iDamageToDo = iDamageToDo + tDangerousFeatures[pPlot:GetFeatureType()];
				print("BLOODMOON: " .. iDamageToDo .. " damage will be inflicted...");
			else
				print("BLOODMOON: Unit is immune to damage from this FeatureType!!!");
			end
		end
		if iDamageToDo > 0 then
			print("BLOODMOON: Inflicting " .. iDamageToDo .. " damage from terrain and features.");
			pUnit:SetDamage(pUnit:GetDamage() + iDamageToDo, iPlayerID);
		end
	end
end
GameEvents.PlayerDoTurn.Add(BMC_DoDangerousPlotCheck);

BMC_ListDangerousPlotTypes();