print("BLOODMOON: Loaded bmc_resourceguardians.lua...");

local tResourceGuardians = {};

function BMC_ListResourceGuardians()
	print("BLOODMOON: Clearing tResourceGuardians...");
	tResourceGuardians = {};
	print("BLOODMOON: Listing resource guardians...");
	for row in GameInfo.Resources() do
		if row.HasGuardian == 1 then
			print("BLOODMOON: All " .. row.Type .. " should be guarded by " .. row.GuardianUnitType .. "...");
			tResourceGuardians[row.Type] = row.GuardianUnitType;
		end
	end
	BMC_PlaceResourceGuardians();
end

function BMC_PlaceResourceGuardians()
	print("BLOODMOON: Placing guardians on resources...");
	local pBarbPlayer = Players[63];
	for sResourceType, sGuardianUnitType in pairs(tResourceGuardians) do
		print("BLOODMOON: Placing " .. sGuardianUnitType .. " on all " .. sResourceType .. "...");
		for iPlotID = 0, Map.GetNumPlots() - 1, 1 do
			local pPlot = Map.GetPlotByIndex(iPlotID);
			if pPlot:GetResourceType() == GameInfoTypes[sResourceType] and pPlot:GetUnit() == nil then
				print("BLOODMOON: Found " .. sResourceType .. " at " .. pPlot:GetX() .. "," .. pPlot:GetY() .. "...");
				pBarbPlayer:InitUnit(GameInfoTypes[sGuardianUnitType], pPlot:GetX(), pPlot:GetY());
				print("BLOODMOON: Placed " .. sGuardianUnitType .. " at " .. pPlot:GetX() .. "," .. pPlot:GetY());
			end
		end
	end
end

BMC_ListResourceGuardians();