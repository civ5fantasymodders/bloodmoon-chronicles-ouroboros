--think Awake in the Night Land
INSERT INTO Domains (Type,Description) VALUES ('DOMAIN_INFERNAL_WATCHER', 'Infernal Watcher Special Domain');
INSERT INTO UnitClasses (Type,Description,MaxGlobalInstances,DefaultUnit) VALUES ('UNITCLASS_INFERNAL_WATCHER', 'Infernal Watcher', 4, 'UNIT_INFERNAL_WATCHER');
INSERT INTO Units (Type,Class,Description,Strategy,Help,Range,RangedCombat,Cost,Moves,CombatClass,Domain,