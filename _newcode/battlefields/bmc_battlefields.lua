print("Loaded BMC_Battlefields.lua...");

-- when units die, save the unit type and plot where they died
-- when 3 units have died within 3 tiles of each other, place a battlefield marker in the center of where they died and save them and their death plots to the battlefield marker
-- when a unit dies within 3 tiles of a battlefield marker, save it and its death plot to the battlefield
-- when a necromancer tries to raise the dead from a battlefield (he must be standing on one), choose a random one of the dead units and plop it on its death plot

local iBattlefield = GameInfoTypes["IMPROVEMENT_CITY_RUINS"];
local tDeadUnits = {};--dead units not saved in battlefields, indexes are a table with X,Y coords, values are unittypes
local tBattlefieldUnits = {};--dead units saved in battlefields, indexes are a table with X,Y coords, values are tables with coords and unittypes

-- sample tables
-- tBattlefieldUnits
-- {1,2} -> {{3,4}->{gameinfotypes.unit_warrior}, {2,3}->{gameinfotypes.unit_spearman}}

-- tDeadUnits
-- {3,4}->{gameinfotypes.unit_warrior}

function BMC_CreateBattlefield(tUnitsToAdd, iX, iY)
	print("Trying to create a battlefield at " .. iX .. "," .. iY .. "...");
	local pPlot = Map.GetPlot(iX, iY);
	if pPlot:GetImprovementType() == nil then
		print("Creating battlefield at " .. iX .. "," .. iY .. "...");
		pPlot:SetImprovementType(iBattlefield);
		tBattlefieldUnits[{iX,iY}] = tUnitsToAdd;
		print("Created battlefield at " .. iX .. "," .. iY);
	end
end

function BMC_AddToBattlefield(iUnitType, iX, iY, iUnitX, iUnitY)
	local pPlot = Map.GetPlot(iX, iY);
	if tBattlefieldUnits[{iX,iY}] ~= nil then
		local tDeadBattlefieldUnits = tBattlefieldUnits[{iX,iY}];
		tDeadBattlefieldUnits[{iUnitX,iUnitY}] = iUnitType;
	end
end

-- still need to catch unit death and either add to tDeadUnits, call BMC_AddToBattlefield, or call BMC_CreateBattlefield