DELETE FROM Technologies WHERE Type NOT IN ('TECH_AGRICULTURE', 'TECH_FUTURE_TECH');
DELETE FROM Processes WHERE Type NOT IN ('PROCESS_WEALTH');
DELETE FROM Eras WHERE Type NOT IN ('ERA_ANCIENT', 'ERA_CLASSICAL', 'ERA_MEDIEVAL', 'ERA_RENAISSANCE', 'ERA_INDUSTRIAL');

-- TIDY UP
DELETE FROM Technology_ORPrereqTechs;
DELETE FROM Technology_PrereqTechs;
DELETE FROM Technology_DomainExtraMoves WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Technology_Flavors WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Technology_FreePromotions WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Technology_TradeRouteDomainExtraRange WHERE TechType NOT IN (SELECT Type FROM Technologies);

DELETE FROM HandicapInfo_AIFreeTechs WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Improvement_TechFreshWaterYieldChanges WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Improvement_TechNoFreshWaterYieldChanges WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Improvement_TechYieldChanges WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM Route_TechMovementChanges WHERE TechType NOT IN (SELECT Type FROM Technologies);
DELETE FROM UnitPromotions_Terrains WHERE PassableTech NOT IN (SELECT Type FROM Technologies);

UPDATE Beliefs SET SpreadModifierDoublingTech=NULL WHERE SpreadModifierDoublingTech NOT IN (SELECT Type FROM Technologies);
UPDATE BuildFeatures SET PrereqTech=NULL;
UPDATE Buildings SET PrereqTech=NULL WHERE PrereqTech NOT IN (SELECT Type FROM Technologies);
UPDATE Buildings SET ObsoleteTech=NULL WHERE ObsoleteTech NOT IN (SELECT Type FROM Technologies);
UPDATE Buildings SET EnhancedYieldTech=NULL WHERE EnhancedYieldTech NOT IN (SELECT Type FROM Technologies);
UPDATE Processes SET TechPrereq=NULL WHERE TechPrereq NOT IN (SELECT Type FROM Technologies);
UPDATE Resolutions SET TechPrereqAnyMember=NULL WHERE TechPrereqAnyMember NOT IN (SELECT Type FROM Technologies);
UPDATE Resources SET TechCityTrade=NULL WHERE TechCityTrade NOT IN (SELECT Type FROM Technologies);
UPDATE Resources SET TechReveal=NULL WHERE TechReveal NOT IN (SELECT Type FROM Technologies);
UPDATE UnitPromotions SET TechPrereq=NULL WHERE TechPrereq NOT IN (SELECT Type FROM Technologies);
UPDATE Units SET PrereqTech=NULL WHERE PrereqTech NOT IN (SELECT Type FROM Technologies);
UPDATE Units SET ObsoleteTech=NULL WHERE ObsoleteTech NOT IN (SELECT Type FROM Technologies);

UPDATE Processes SET TechPrereq=NULL;
DELETE FROM Process_Flavors WHERE ProcessType NOT IN (SELECT Type FROM Processes);
DELETE FROM Process_ProductionYields WHERE ProcessType NOT IN (SELECT Type FROM Processes);

UPDATE LeagueProjects SET Process=NULL WHERE Process NOT IN (SELECT Type FROM Processes);

-- ERA_ANCIENT, ERA_INDUSTRIAL and ERA_MODERN are referenced in PostDefines
DELETE FROM Era_CitySoundscapes WHERE EraType NOT IN (SELECT Type FROM Eras);
DELETE FROM Era_NewEraVOs WHERE EraType NOT IN (SELECT Type FROM Eras);
DELETE FROM Era_Soundtracks WHERE EraType NOT IN (SELECT Type FROM Eras);
DELETE FROM Belief_EraFaithUnitPurchase WHERE EraType NOT IN (SELECT Type FROM Eras);

UPDATE Beliefs SET ObsoleteEra=NULL WHERE ObsoleteEra NOT IN (SELECT Type FROM Eras);
UPDATE Resources SET WonderProductionModObsoleteEra=NULL WHERE WonderProductionModObsoleteEra NOT IN (SELECT Type FROM Eras);

UPDATE Buildings SET FreeStartEra='ERA_INDUSTRIAL' WHERE FreeStartEra NOT IN (SELECT Type FROM Eras);
UPDATE PolicyBranchTypes SET EraPrereq='ERA_ANCIENT' WHERE EraPrereq NOT IN (SELECT Type FROM Eras);

DELETE FROM GreatWorks WHERE EraType NOT IN (SELECT Type FROM Eras);

-- DELETE FROM DeleteTechnologies;