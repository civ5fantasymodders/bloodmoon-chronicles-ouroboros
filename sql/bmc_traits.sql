INSERT INTO Traits (Type) VALUES
				   ('TRAIT_VANDARIA'),
				   ('TRAIT_KIZU'),
				   ('TRAIT_KROSVIK'),
				   ('TRAIT_ISMALIA'),
				   ('TRAIT_ILADOREA'),
				   ('TRAIT_GVORANDIA'),
				   ('TRAIT_AKKA'),
				   ('TRAIT_MULUSH'),
				   ('TRAIT_RUKLUSH'),
				   ('TRAIT_UNDEAD'),
				   ('TRAIT_CELESTIALS'),
				   ('TRAIT_INFERNALS');
UPDATE Traits SET Description = 'TXT_KEY_BMC_' || Type;
UPDATE Traits SET ShortDescription = Description || '_TITLE';
CREATE TABLE IDRemapper ( id INTEGER PRIMARY KEY AUTOINCREMENT, Type TEXT );
INSERT INTO IDRemapper (Type) SELECT Type FROM Traits ORDER BY ID;
UPDATE Traits SET ID =	( SELECT IDRemapper.id-1 FROM IDRemapper WHERE Traits.Type = IDRemapper.Type);
DROP TABLE IDRemapper;
-- INSERT INTO Trait_FreeResourceFirstXCities (TraitType,			ResourceType,		ResourceQuantity,	NumCities) VALUES
										   -- ('TRAIT_KROSVIK',	'RESOURCE_IRON',	2,					1),
										   -- ('TRAIT_ISMALIA',	'RESOURCE_HORSE',	4,					1),
										   -- ('TRAIT_GVORANDIA',	'RESOURCE_IRON',	4,					1);